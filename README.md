﻿# 2180607542 - Phạm Hùng #

# UserStory #
| **Title**               | Delete subject information                      |
| ----------------------- | ----------------------------------------------- |
| **Value Statement**     |   -  As a user, I want to delete subject classes to update my subject class information | 
|                         |   - Delete subject classes                      |
| **Acceptance Criteria** |   - Given that the class information need to be deleted |
|						  |	  - When the user is logged into the system with their account |
|                         |   - When users search and select the subject class that needs to be deleted to update information |
|                         |   - Then the system must display all information about that subject class |
|                         |   - And user can delete the searched subject class |
|                         |   - The user must confirm when they have completed deleting the subject class |
|                         |   - Then the system needs to update subject class information after the user has deleted it |
|                         |   - Then the system must notify the user that updating the subject class information has been successful |
|                         |   - Ensure that updating subject class information can be conveniently done from the user interface |
| **Definition of Done**  |   * Acceptance Criteria Met                     |
|                         |   * Test Cases Passed                           |
|                         |   * Code Reviewed                               |
|                         |   * Package Into Specific Folders               |
|                         |   * Synchronize Folders Between Devices         | 
|                         |   * Product Owner Accepts User Story            |
| **Owner**               |    Phạm Hùng                                    |
| **Interation**          |   * Unscheduled                                 |
| **Estimate**            |   * 5 points                                    |
